import { alternator, once, onceAndAfter, thisManyTimes } from './once';

describe('once', () => {
  const fn = jest.fn();

  it('should allow call to incoming function only once', () => {
    const hoc = jest.fn(once(fn));

    hoc();
    hoc();
    hoc();

    expect(hoc).toBeCalledTimes(3);
    expect(fn).toBeCalledTimes(1);
  });

  it('should pass arguments to incoming function', () => {
    once(fn)('test', 'me');

    expect(fn).toHaveBeenCalledWith('test', 'me');
  });
});

describe('onceAndAfter', () => {
  const fn1 = jest.fn();
  const fn2 = jest.fn();

  it('should allow call to first incoming function only once', () => {
    const hoc = jest.fn(onceAndAfter(fn1));

    hoc();
    hoc();
    hoc();

    expect(hoc).toBeCalledTimes(3);
    expect(fn1).toBeCalledTimes(1);
  });

  it('should pass arguments to incoming function', () => {
    const hoc = jest.fn(onceAndAfter(fn1, fn2));

    hoc('test', 'me');
    hoc('test', 'me');

    expect(fn1).toHaveBeenCalledWith('test', 'me');
    expect(fn2).toHaveBeenCalledWith('test', 'me');
  });

  it('should call second incoming function if first is done', () => {
    const hoc = jest.fn(onceAndAfter(fn1, fn2));

    hoc();
    expect(fn1).toBeCalledTimes(1);
    expect(fn2).toBeCalledTimes(0);

    hoc();
    expect(fn1).toBeCalledTimes(1);
    expect(fn2).toBeCalledTimes(1);

    hoc();
    expect(fn1).toBeCalledTimes(1);
    expect(fn2).toBeCalledTimes(2);
  });
});

describe('alternator', () => {
  const fn1 = jest.fn();
  const fn2 = jest.fn();

  it('should alternatively call first and second incoming function', () => {
    const hoc = alternator(fn1, fn2);

    hoc();
    expect(fn1).toHaveBeenCalled();
    expect(fn2).not.toHaveBeenCalled();

    jest.resetAllMocks();

    hoc();
    expect(fn1).not.toHaveBeenCalled();
    expect(fn2).toHaveBeenCalled();

    jest.resetAllMocks();

    hoc();
    expect(fn1).toHaveBeenCalled();
    expect(fn2).not.toHaveBeenCalled();

    jest.resetAllMocks();

    hoc();
    expect(fn1).not.toHaveBeenCalled();
    expect(fn2).toHaveBeenCalled();
  });

  it('should pass arguments to incoming function', () => {
    const hoc = alternator(fn1, fn2);

    hoc('test', 'me');
    hoc('test', 'me');

    expect(fn1).toHaveBeenCalledWith('test', 'me');
    expect(fn2).toHaveBeenCalledWith('test', 'me');
  });
});

describe('thisManyTimes', () => {
  const fn = jest.fn();

  it('should allow call to incoming function n times', function () {
    const hoc = jest.fn(thisManyTimes(fn, 2));

    hoc();
    hoc();
    hoc();
    hoc();
    hoc();

    expect(hoc).toBeCalledTimes(5);
    expect(fn).toBeCalledTimes(2);
  });

  it('should pass arguments to incoming function', () => {
    thisManyTimes(fn, 1)('test', 'me');

    expect(fn).toHaveBeenCalledWith('test', 'me');
  });
});
