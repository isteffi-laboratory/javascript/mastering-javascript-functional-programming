import { Function, HigherOrder1, HigherOrder2 } from './once.types';

export const once: HigherOrder1 = (fn: Function) => (...args) => {
  fn && fn(...args);
  fn = null;
};

export const onceAndAfter: HigherOrder1 = (
  fn1: Function,
  fn2: Function = () => {}
) => (...args) => {
  fn1(...args);
  fn1 = fn2;
};

export const alternator: HigherOrder1 = (fn1: Function, fn2: Function) => (
  ...args
) => {
  fn1(...args);
  [fn1, fn2] = [fn2, fn1];
};

export const thisManyTimes: HigherOrder2 = (fn: Function, limit: number) => {
  return (...args) => (limit-- > 0 ? fn(...args) : () => {});
};
