export type HigherOrder1 = (fn1: Function, fn2?: Function) => Function;
export type HigherOrder2 = (fn1: Function, n: number) => Function;

export type Function = <T>(...args: T[]) => void;
