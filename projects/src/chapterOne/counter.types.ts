export type Counter = (count?: number) => () => number;
