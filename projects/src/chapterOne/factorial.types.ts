export type N = number;
export type Factorial = (n: N, m?: number) => number;
