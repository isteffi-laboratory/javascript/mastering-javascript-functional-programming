import { Counter } from './counter.types';

export const counter: Counter = (count = 0) => () => ++count;
