import { Factorial, N } from './factorial.types';

const prohibitNegatives = (n: N, fn: Factorial) => {
  if (n < 0) {
    throw new Error('Negative integers are not allowed.');
  }
  return fn(n);
};

const factorialUpward: Factorial = (n, m = 1) =>
  n === m ? n : m * factorialUpward(n, m + 1);

const factorialDownward: Factorial = n =>
  n === 0 ? 1 : n * factorialDownward(n - 1);

export const factU = n => prohibitNegatives(n, factorialUpward);
export const factD = n => prohibitNegatives(n, factorialDownward);
