import { factD, factU } from './factorial';
import { strict as assert } from 'assert';

describe('factorial downward', () => {
  it('should return factorial of an integer', () => {
    expect(factD(5)).toBe(120);
  });

  it('should only allow non-negative integers', () => {
    assert.throws(() => factD(-5), {
      message: 'Negative integers are not allowed.',
    });
  });

  it('should return 1 for 0', () => {
    expect(factD(0)).toBe(1);
  });
});

describe('factorial upward', () => {
  it('should return factorial of an integer', () => {
    expect(factU(5)).toBe(120);
  });

  it('should only allow non-negative integers', () => {
    assert.throws(() => factU(-5), {
      message: 'Negative integers are not allowed.',
    });
  });

  it('should return 1 for 0', () => {
    expect(factD(0)).toBe(1);
  });
});
