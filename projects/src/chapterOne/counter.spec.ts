import { counter } from './counter';

describe('counter', () => {
  it('should increase and return count', () => {
    const count = counter();
    expect(count()).toBe(1);
    expect(count()).toBe(2);
    expect(count()).toBe(3);
  });
});
