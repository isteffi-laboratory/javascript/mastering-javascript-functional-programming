function first() {
  // hoisted => accessible everywhere
}

const second = function () {
  // not hoisted => only accessible after the assignment
};

const third = function someName() {
  // same as second
  // more easily recognized in an error traceback
};

const fourth = (function () {
  // lets you use a closure
  // inner function can use variables or other functions,
  // defined in its outer function
  return function () {};
})();

const fifth = new Function();

const sixth = () => {};
